set nocompatible              " required
filetype off                  " required
set background=dark

" set the runtime path to include Vundle and initialize
call plug#begin('~/.nvim/nvim-plugins')

" add all your plugins here

Plug 'tmhedberg/SimpylFold'
Plug 'vim-scripts/indentpython.vim'
Plug 'kien/ctrlp.vim'
Plug 'jnurmine/Zenburn'
Plug 'sickill/vim-monokai'
Plug 'vim-syntastic/syntastic'
Plug 'nvie/vim-flake8'
Plug 'elixir-editors/vim-elixir'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'morhetz/gruvbox'
Plug 'elzr/vim-json'
Plug 'editorconfig/editorconfig-vim'

" ...

" All of your Plugins must be added before the following line
call plug#end()            " required

filetype plugin indent on    " required

" commented out since using EditorConfig plugin
"au BufNewFile,BufRead *.py:
"    \ setlocal tabstop=4
"    \ setlocal softtabstop=4
"    \ setlocal shiftwidth=4
"    \ setlocal textwidth=79
"    \ setlocal expandtab
"    \ setlocal autoindent
"    \ setlocal fileformat=unix

" set numbering
set nu
" set colorscheme
colorscheme monokai

let python_highlight_all=1
syntax on

" for folding code blocks
nnoremap <space> za

" set status line theme
let g:airline_theme='behelit'
